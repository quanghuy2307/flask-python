from app.todo.models.todo import Todo
from app.token.models.token import Token
from app.user.models.user import User
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
import unittest
from app import create_app, db


app = create_app()
app.app_context().push()

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)


@manager.command
def run():
  app.run()


@manager.command
def test():
  """ Runs the unit tests """
  tests = unittest.TestLoader().discover('app/test', pattern='test*.py')
  result = unittest.TextTestRunner(verbosity=2).run(tests)
  if result.wasSuccessful():
    return 0
  return 1


if __name__ == '__main__':
  manager.run()
  # db.create_all() # Tạo bảng trong CSDL
