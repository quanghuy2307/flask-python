from app.database.models import Base
from app import db


class Todo(Base):
  __tablename__ = 'todo'

  task_name = db.Column(db.Text, nullable=False)
  start_at = db.Column(db.DateTime, nullable=False)
  progress = db.Column(db.Integer, nullable=False, server_default=db.text("0"))

  def __init__(self, task_name, start_at, progress):
    self.task_name = task_name
    self.start_at = start_at
    self.progress = progress
