from app.database.models import Base
from app import db


class Token(Base):
  __tablename__ = 'token'

  hashed_value = db.Column(db.Text, nullable=False)
  type = db.Column(db.Text, nullable=False)
  expired_in = db.Column(db.Integer, nullable=False)
  is_expired = db.Column(db.Boolean, nullable=False, server_default=db.text('False'))

  def __init__(self, hashed_value, type, expired_in, is_expired):
    self.hashed_value = hashed_value
    self.type = type
    self.expired_in = expired_in
    self.is_expired = is_expired
