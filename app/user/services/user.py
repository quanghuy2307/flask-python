from app import db, bcrypt
from app.user.models.user import User
from flask import request
from app.util import make_success_response


def service_get_user_by_id(userId):
  return User.query.get(userId)


def service_get_all_user():
  return User.query.all()


def service_create_user():
  data = request.get_json()

  if not ('fullname' in data and "password" in data and "email" in data and "phonenumber" in data and "address" in data and "birthday" in data):
    return make_success_response(400, "Dữ liệu đầu vào không hợp lệ")

  new_user = User(
    data["fullname"], 
    bcrypt.generate_password_hash(data["password"]).decode('utf-8'), 
    data["email"], 
    data["phonenumber"], 
    data["address"], 
    data["birthday"]
  )

  try:
    db.session.add(new_user)
    db.session.commit()

    return make_success_response(201, "Tạo người dùng thành công")
  except Exception:
    db.session.rollback()

    return make_success_response(500, "Đã có lỗi xảy ra")


def service_update_user_by_id(userId):
  user = User.query.get(userId)

  if not user:
    return make_success_response(404, "Người dùng không tồn tại")

  data = request.json

  if not ('fullname' in data and "email" in data and "phonenumber" in data and "address" in data and "birthday" in data):
    return make_success_response(400, "Dữ liệu đầu vào không hợp lệ")

  user.email = data["email"]
  user.address = data["address"]
  user.birthday = data["birthday"]
  user.fullname = data["fullname"]
  user.phonenumber = data["phonenumber"]

  try:
    db.session.commit()

    user = User.query.get(userId)

    return make_success_response(200, "Cập nhật thông tin người dùng thành công", user)
  except Exception:
    db.session.rollback()

    return make_success_response(500, "Đã có lỗi xảy ra")


def service_update_user_password_by_id(userId):
  user = User.query.get(userId)

  if not user:
    return make_success_response(404, "Người dùng không tồn tại")

  data = request.json

  if not ('current_password' in data and "new_password" in data and 're_new_password' in data):
    return make_success_response(400, "Dữ liệu đầu vào không hợp lệ")

  if data["new_password"] != data["re_new_password"]:
    return make_success_response(400, "Mật khẩu mới nhập lại phải giống với mật khẩu mới")

  if not bcrypt.check_password_hash(user.hashed_password, data["current_password"]):
    return make_success_response(400, "Mật khẩu hiện tại không chính xác")

  user.hashed_password = bcrypt.generate_password_hash(data["new_password"]).decode('utf-8')

  try:
    db.session.commit()

    user = User.query.get(userId)

    return make_success_response(200, "Cập nhật mật khẩu người dùng thành công", user)
  except Exception:
    db.session.rollback()

    return make_success_response(500, "Đã có lỗi xảy ra")


def service_delete_user_by_id(userId):
  user = User.query.get(userId)

  if not user:
    return make_success_response(404, "Người dùng không tồn tại")

  try:
    db.session.delete(user)
    db.session.commit()

    return make_success_response(200, "Xóa người dùng thành công")
  except Exception:
    db.session.rollback()

    return make_success_response(500, "Đã có lỗi xảy ra")
