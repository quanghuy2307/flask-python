from app.database.models import Base
from app import db


class User(Base):
  __tablename__ = 'user'

  fullname = db.Column(db.Text, nullable=False)
  hashed_password = db.Column(db.Text, nullable=False)
  email = db.Column(db.Text, nullable=False)
  phonenumber = db.Column(db.Text, nullable=True)
  address = db.Column(db.Text, nullable=True)
  birthday = db.Column(db.Date, nullable=True)

  def __init__(self, fullname, hashed_password, email, phonenumber, address, birthday):
    self.fullname = fullname
    self.hashed_password = hashed_password
    self.email = email
    self.phonenumber = phonenumber
    self.address = address
    self.birthday = birthday

  def __repr__(self):
    return '<User {} {} {} {} {}>'.format(self.fullname, self.email, self.phonenumber, self.address, self.birthday)
