from flask import Blueprint

from app.util import make_success_response
from ..services.user import (service_create_user, service_delete_user_by_id, service_get_user_by_id,
                             service_get_all_user, service_update_user_by_id, service_update_user_password_by_id)
from werkzeug.exceptions import NotFound

user_controller = Blueprint('user_controller', __name__)


@user_controller.route("/create", methods=['POST'])
def createUserController():
  return service_create_user()


@user_controller.route("/get-by-id/<int:userId>", methods=['GET'])
def getUserByIdController(userId):
  user = service_get_user_by_id(userId)

  if not user:
    raise NotFound(description="Người dùng không tồn tại")
  else:
    return make_success_response(200, "Lấy thông tin người dùng thành công", user)


@user_controller.route("/get-all", methods=['GET'])
def getAllUserController():
  users = service_get_all_user()

  return make_success_response(200, "Lấy danh sách thông tin người dùng thành công", users)


@user_controller.route("/update-by-id/<int:userId>", methods=['PUT'])
def updateUserByIdController(userId):
  return service_update_user_by_id(userId)


@user_controller.route("/update-password-by-id/<int:userId>", methods=['PUT'])
def updateUserPasswordByIdController(userId):
  return service_update_user_password_by_id(userId)


@user_controller.route("/delete/<int:userId>", methods=['DELETE'])
def deleteUserByIdController(userId):
  return service_delete_user_by_id(userId)
