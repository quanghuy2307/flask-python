from flask import jsonify
from .util import object_to_dict, objects_to_dicts


def make_success_response(status_code: int, code: int, message: str, data: any = None):
  res = None

  if data:
    if isinstance(data, list):
      print('asdfhgjhkl;ljhgfdsadsfghjkjhgfdsfgjhklkjhgfdghjkl')
      res = objects_to_dicts(data)
    elif isinstance(data, object):
      res = object_to_dict(data)

  return jsonify({
      'code': code,
      'message': message,
      'data': res
  }), status_code
