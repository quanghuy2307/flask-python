from sqlalchemy import inspect


def object_to_dict(obj):
  if not obj:
    return None

  return {c.key: getattr(obj, c.key) for c in inspect(obj).mapper.column_attrs}


def objects_to_dicts(objs):
  if not objs:
    print('nksfjksfnkfnjnksfjksfnkfnjnksfjksfnkfnjnksfjksfnkfnjnksfjksfnkfnj')
    return []

  return [object_to_dict(obj) for obj in objs]
