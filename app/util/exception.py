from flask import jsonify
from werkzeug.exceptions import HTTPException


def make_exception_response(error: Exception):
  # Xử lý lỗi HTTPException
  if isinstance(error, HTTPException):
    return jsonify({
        "message": error.description,
        "code": error.code
    }), error.code

  # Xử lý lỗi khác
  else:
    return jsonify({
        "message": "Đã có lỗi xảy ra",
        "code": 500
    }), 500

