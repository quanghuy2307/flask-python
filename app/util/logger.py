import logging

logging.basicConfig(format='%(asctime)-15s %(name)s %(levelname)-8s %(message)s')


def make_logger(name):
  return logging.getLogger(name)
