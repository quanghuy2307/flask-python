from app import db


class Base(db.Model):
  __abstract__ = True

  id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
  created_at = db.Column(db.DateTime, nullable=True, server_default=db.func.now())
  updated_at = db.Column(db.DateTime, nullable=True, server_default=db.func.now(), server_onupdate=db.func.now())
  deleted_at = db.Column(db.DateTime, nullable=True)
  created_id = db.Column(db.Integer, nullable=True)
  updated_id = db.Column(db.Integer, nullable=True)
