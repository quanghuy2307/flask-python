from dotenv import load_dotenv
from app.util import make_exception_response
from config import environments
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from flask_marshmallow import Marshmallow
from flask_bcrypt import Bcrypt
import os

db = SQLAlchemy()
ma = Marshmallow()
bcrypt = Bcrypt()


def create_app():
  # Khởi tạo app
  app = Flask(__name__)

  # Khởi tạo ENV
  load_dotenv()
  app.config.from_object(environments[os.getenv('SERVER_ENV')])

  # Khởi tạo bcrypt
  bcrypt.init_app(app)

  # Khởi tạo DB
  db.init_app(app)

  # Đăng ký error handler
  # @app.errorhandler(Exception)
  # def exception_response(error: Exception):
  #   return make_exception_response(error)

  # Test
  @app.route('/')
  def index():
    return "<p>Hello World!</p>"

  # Đăng ký route
  from app.user.controllers import user_controller
  app.register_blueprint(user_controller, url_prefix='/user')

  # from app.token.controllers import tokenController
  # app.register_blueprint(tokenController, url_prefix='/token')

  # from app.todo.controllers import todoController
  # app.register_blueprint(todoController, url_prefix='/todo')

  return app
