from .config import ProductionEnvironment, DevelopmentEnvironment, TestingEnvironment

environments = {
    'default': DevelopmentEnvironment,
    'development': DevelopmentEnvironment,
    'testing': TestingEnvironment,
    'production': ProductionEnvironment
}
