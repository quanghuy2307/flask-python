import os
from dotenv import load_dotenv


load_dotenv()


""" Môi trường chung """
class CommonEnvironment:
  JWT_ACCESS_TOKEN_SECRET_KEY = os.getenv('JWT_ACCESS_TOKEN_SECRET_KEY')
  JWT_REFRESH_TOKEN_SECRET_KEY = os.getenv('JWT_REFRESH_TOKEN_SECRET_KEY')
  JWT_ACCESS_TOKEN_EXPIRED_IN = os.getenv('JWT_ACCESS_TOKEN_EXPIRED_IN')
  JWT_REFRESH_TOKEN_EXPIRED_IN = os.getenv('JWT_REFRESH_TOKEN_EXPIRED_IN')
  SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")

""" Môi trường phát triển """
class DevelopmentEnvironment(CommonEnvironment):
  DEBUG = True
  # TESTING = True
  SQLALCHEMY_ECHO = True
  SQLALCHEMY_TRACK_MODIFICATIONS = True

""" Môi trường test """
class TestingEnvironment(CommonEnvironment):
  DEBUG = False
  TESTING = True

""" Môi trường sản phẩm """
class ProductionEnvironment(CommonEnvironment):
  DEBUG = False
  TESTING = False
